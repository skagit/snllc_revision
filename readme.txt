Original data, for both 25 and 125 agent versions are present in the transparently named directories.

Summary data, which has been reduced by averaging over all signals present after intergenerational transfer, and further expanded by adding degree and local transitivity data, is present in the transparently named directory.

The summary.r file provides code to load the original signal and network data, transform the naming of columns and factors, compute network and complexity statistics, and merge and output the two csv files present in summary.

The analysis.r file provides code to load and prep the summary data frames, and demonstrate data.table syntax for aggregating such large data sets (as opposed to the plyr/dplyr syntax we had been using prior).

The column names are as follows:
  network_id -- each topology was replicated 100 times, and this is the replication id
  agent_id -- uniqely identifies agent accross all generations of a given replication
  topology -- the name of the social network as a factor, or numeric value in case of the transitivity networks
  complexity -- average reanalysis of all signals agent acquired immediately after intergenerational transfer
  degree -- edge degree of agent in corresponding network (each network is uniquely identified by the network_id and topology)
  transitivity_local -- local clustering coeficient of the agent in corresponding network
