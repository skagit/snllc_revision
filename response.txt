Dear [Luca: do we address to Spivy, or general editor again?],

Thank you so much for your consideration of our article and the insightful comments we received from the reviewers. As requested, we have revised the article to reflect these, and provide a one-by-one account of them below.

### Reviewer One Responses

"1. I found the provided examples of grammaticalization helpful but please explain more explicitly why greater reanalysis leads to more complex morphology. Couldn't it work the other way around such that "ain't" is re-analyzed to the morphologically simpler "are not"?"

A new section has been added (lines 67-154) that now extensively reviews the fundamental principles of grammaticalization, and morphological development, including a subsection describing the kind of non-reversal of changes queried about above.

"2. Experiment (i.e., Simulation) 2 is very useful, but I have several concerns about it. First, the intent of this simulation seems to be to distinguish between the effects of the clustering coefficient and other aspects of network topology such as degree distribution and hierarchy depth. However at present these two factors are not being manipulated independently from one another. For example, the hierarchical network is both more hierarchical than the random network and has higher clustering (i.e., transitivity). Wouldn't it make sense to compare a Hierarchical network with transitivity of 0.4 to a random network with a transitivity of 0.4. Also, why does the BA network have a transitivity of 0? Scale-free networks can have different degrees of clustering (e.g., see https://arxiv.org/pdf/1105.3347.pdf ).  Third, are large networks necessarily scale-free? (as is claimed on p. 7)?."

While the clustering coefficient is manipulatable using new models, such as the one in the provided paper, there are some crucial limitations. In particular, the difficulty lies in the distribution of the clustering in agents of different degrees. In human social networks, the density of connections is concentrated in the less connected, more peripheral nodes; local transitivity varies inversely with degree. Accounting for this property in current models is an ongoing issue, and we summarize several current approaches and their limits in lines 377-393.

As for the comparison, the outcome for a random network with transitivity of 0.4 is already produced in Simulation 1, and we explicitly reference it in stating our predictions for Hierarchical in lines 395-404.

Finally, large social networks as empirically observed in the wild are scale-free, but the empirical evidence is mostly drawn from online social media networks (e.g. Facebook) and collaboration networks (e.g. Web of Science), and so different kinds of social connections not discussed in the paper may have non-scale-free distributions. We have addressed these concerns with additional references in lines 370, and 380-381.


"3. The simulations reported in the main text use 25 agents. The simulation in the supplementary materials uses 125 agents and comes to some (though not entirely) different conclusions. This is troublesome as the difference between 25 and 125 is (presumably) inconsequential as applied to actual languages. The real world differences between so-called "small" and "large" languages correspond to differences between languages spoken by a few thousand people and languages spoken by a few million (i.e., orders of three-magnitudes and more). The authors need to address this point. If their results are sensitive to an increase in population from 25 to 125, how well can they explain real world phenomena?"

The simulations are small in terms of raw agent number, 25 and 125 respectively, but differ in orders of magnitude in terms of hierarchical depth and edge quantity. Additionally, for some network topologies an increase in size would likely violate findings on the limits of human capacity to maintain relationships. We have now added a third simulation section that accounts for these differences and tests them statistically in lines 459-488. That said, we also acknowledge the computational limits of running are model at larger sizes that presented, and mention this limitation.

"4. ANOVA is the wrong analysis to use for the first simulation. The predictors are continuous. Please use a regression."

Our original ANOVA analysis have been replaced with mixed-effects models, and the parameters reported in full in the results sections.

"a. Graphs with many color-coded lines (like Figure 1) are hard to read (e.g., the colors for 1.0 and 0.1 are quite similar). Please label the lines directly on the graph rather than with colors (check out https://cran.r-project.org/web/packages/ggrepel/vignettes/ggrepel.html). Relatedly, it would be useful to run each condition multiple times and average the results to have smoother trajectories."

Because of the overlapping in the conditions, even with line labels the graph is still difficult to read. Instead we have increased the thickness of the lines and legend materials to make them easier to read. If this is still not sufficient, perhaps we can add elongated figures in the supplemental materials with full page graphs for clear visualization. For the narrative of the paper, though, the overlap does not detract.

The results were drawn from 100 replications per condition in the 25-agent networks, and 50 replications per condition in the 125-agent networks.

"b. Please include a visualization and formal analyses of: (1) the nonlinear relationship between transitivity and clustering (e.g., a graph showing reanalysis on the y-axis and transitivity on the x-axis). (2) Differences in the "flow of signals" as described in sect 3.3"

The simplistic "flow of signals" explanation has been replaced with an extended analysis of how complexity levels change overtime with respect to the local transitivity of agents in lines 319-349. A graph showing the relationship between transitivity and mean level of reanalysis has been added to the supplemental materials and referenced in the text. If required, and equation can be estimated to describe the curve, but it is immaterial to the narrative of the text.

"c. Please check over the references. There are systematic formatting errors for the in-text cites and inconsistencies in the reference list (e.g., incorrect titles, all caps, etc.)"

The all caps item has been corrected, and the titles have been checked using Google Scholar.

"d. Please carefully proofread the paper for typos and confusing metonymy, e.g., "..a single hub reduces the complexity to Random levels" should read "..a single hub reduces complexity to levels observed in the Randomly-connected network"

The language conflating network names and descriptions has been removed.

e. The condition that corresponds to the "random" network in Figure 3 is mistakenly labeled "giant"

This error has been corrected. 


### Reviewer Two Responses

"The first is the background review — the authors miss a number of existing studies that relate closely to their goals and really ought to be cited or discussed in some way."

The requested articles, listed below, have been addressed, with lines containing the citations following each response.

"Second, the simulations themselves are very difficult to follow. I consulted the online materials to see myself, and more is needed in the body of the paper to explain how the language structures work, give clearer and more concrete examples of the simulations, and so on."

The algorithms and abstract data structures have all been described in lines 154-246.

"The authors should include some summary of the following papers..."

Tria, F., Galantucci, B., & Loreto, V. (2012). Naming a structured world: a cultural route to duality of patterning. PLOS one, 7(6), e37744.

A key feature of this work is the ability of agents to navigate communicative deficits by creating new holistic signals, or crucially, reusing existing signals. While our model does not represent signal structures with symbolic units, we track each signal by indexing its original lexical origin (accounting for an initial signal structure) and assigning a new expression value (accounting for current, altered structure) for each reanalysis, keeping track of progressive, individual changes to that signal through intergenerational transfer. Finally, signals that have accrued much reanalysis are periodically replaced, reflecting eventual phonological erosion of signals.

In this way agents can individuate, albeit abstractly, the physical structure of signals from each other, reusing existing material in the case where the origin does not change, or inventing new structure with the creation of paraphrastic signals with novel origins and zero levels of reanalysis.

Citation in line 13.

Bentz, C., & Winter, B. (2013). Languages with more second language learners tend to lose nominal case. Language Dynamics and Change, 3(1), 1-27.

This relates closely to previous work by Lupyan and Dale, in which they propose second language learners as provide a simplifying bias in language complexity. Our model though, explores both how a related means of simplification, namely, paraphrastic constructions lackiung morphology (often used by second language speakers as well) interacts with a means of complexification, repeated reanalysis. Thus we provide a means for languages to both grow and shrink in complexity.

Citation in line 42.

Roberts, G., Lewandowski, J., & Galantucci, B. (2015). How communication changes when we cannot mime the world: Experimental evidence for the effect of iconicity on combinatoriality. Cognition, 141, 52-66.

While our paper does not have an pre-compositional stage, i.e. iconic, as explored in this paper, we do use the bias they describe for compositional forms. Given a equal usage statistics, agents will prefer signals that are more easily composed due to repeated reanalysis.

Citation in line 16.

Lupyan, G., & Dale, R. (2016). Why are there different languages? The role of adaptation in linguistic diversity. Trends in cognitive sciences, 20(9), 649-660.

In this paper Lupyan and Dale highlight the compounding effect of small biases in language change. While we do not address any of the specific biases they present, our exploration of physical network properties is in exactly the same spirit.

Citation in line 54.

Batali, J. (1998). Computational simulations of the emergence of grammar. In J. R. Hurford, M. Studdert-Kennedy, & C. Knight (Eds.), Evolution of language: Social and cognitive bases. Cambridge: Cambridge University Press.

This work complements other work done with symbolic architectures, e.g. that of Simon Kirby, which demonstrates the general principles for compositionality to emerge in communication systems. Our paper, though, tries to deepen that work by distinguishing between two kinds of compositionality used in natural language - syntactic and morphological - and explore what may differentially drive the use of one over the other.

Citation in line 10.

Perfors, A., & Navarro, D. J. (2014). Language evolution can be shaped by the structure of the world. Cognitive science, 38(4), 775-793. Chicago.

An important paper that brings back into scope the importance of information structure on repeated learning. While we do not directly manipulate the meaning space the agents use, the information flow of signals in the system is directly affected by the connection patterns between agents, both in terms of access to signals within the time span of a generation, as well as the diversity of signals encountered.

Citation in line 54. 

"I think this paper needs to be a regular article with the requisite level of methodological detail in the body of the paper so that readers could follow all the computational and structural detail. The nature of the agents and their communication systems is key to these simulations. The paper should flesh this out in a longer proper methods section, taking from the material in their supplemental section — which, I confess, was still not enough for me to follow along. A critical detail is line 188 in their file https://gitlab.com/skagit/SNLLC/blob/master/code/simulation.jl. It appears that the constructions are numeric identifiers that are simply ordered and paired with a set of ordered meanings. I am sorry if I have missed these details, but the fact that I am struggling means that some of these implementational details have to be presented in the body of the paper. It seems to me that reanalysis is therefore a rather abstract process in the model, and does not reflect any
actual manipulation of the signals but only hypothetical "reanalysis" in the event of particular interaction constraints of the agents? I think better expanding on the material on p. 2 in the supplementary section, adding more explicit computational detail, and putting in the main body it would really be clearer. It will also be clear to readers that there is no actual structural reanalysis in this simulation — there are (form, meaning) pairs that are "hypothetically" reanalyzed according to rather direct assumptions built in… this may be a weakness but I cannot exactly tell."

As mentioned above, the simulation details have now been clarified. With regards to reanalysis, no human language is static; structural change is constant, and relates directly to the amount of variation learners encounter during acquisition of the grammar. Our model addresses this at a more abstract level, not distinguishing between semantic vs physical changes, but instead tracks reanalysis itself, as the unidirectional hypothesis makes predictions on how to interpret different levels. Keeping with observations of grammaticalization processes, there is a constant base level of reanalysis present for all agents, which is multiplied on a signal by signal basis depending on the amount of variation and agent receives in the input.

"p. 1, 2nd sentence: When a "categorical divide" is referenced here, it should be made clear that encoding has a kind of categorical division on individual forms, but that language vary widely in their general encoding strategies (in other words, it is not crisply bimodal, the encoding strategy of morphological systems)."

This language has been replaced to highlight the graduated nature.

"General: It needs to me made clearly why the proxy score of reanalysis maps onto morphological complexity, if that is the goal. It is either unclear — or debatable — or both, that the way this is implemented would map perfectly onto conceptions from analysis of language morphologies… more clarity is needed there I think."

In addition to the expanded section on grammaticalization, and extended example from Proto-Indo-European has been introduced to ground how the various subprocesses of grammaticalization interact. An emphasis has been placed on how reanalysis drives the other processes, and can be taken as a proxy given the unidirectional hypothesis. 

"Minor, but important: Dispersion should be clear from the figures. Why not show an error bar over runs of the simulations? A ribbon plot might be nice (e.g., with ggplot2 smooth, since the authors use R in their analysis)."

Adding information in the form of a Loess plot (using the ggplot2 smooth) was attempted, but the shaded regions were virtually undetectable. Error bars have been added to our presentation of interaction effects, but adding dispersion bars over the simulation lines was disruptive to reading the plots. Instead, a dispersion plot of Simulation 1 had been added to the supplementary materials.

Very minor: Citations are missing parentheses.

Citations have been converted to the parenthetical style preferred for APA.

Thank you for your time and consideration,

Matthew Lou-Magnuson & Luca Onnis
